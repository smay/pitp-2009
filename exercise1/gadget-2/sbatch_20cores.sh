#!/bin/bash -l
# standard output and error
##SBATCH --output ./tjob.out.%j
##SBATCH --error ./tjob.err.%j
# initial working directory
##SBATCH --workdir=./
# mail settings
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=smay@mpa-garching.mpg.de
# job parameters
#SBATCH --time=5:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
#SBATCH --job-name=pitp2009_1_20

echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_JOB_NUM_NODES nodes."
echo "Running on $SLURM_NTASKS processors."
echo "Current working directory is $PWD"
echo

srun ./Gadget2 exercise1_20cores.param
