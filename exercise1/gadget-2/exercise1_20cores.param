%  Relevant files

InitCondFile       ICs/exercise1.dat
OutputDir          output_20cores/

EnergyFile         energy.txt
InfoFile           info.txt
TimingsFile        timings.txt
CpuFile            cpu.txt

RestartFile        restart
SnapshotFileBase   snapshot

OutputListFilename output_list.txt  % ignored if OutputListOn = 0


% CPU time limit

TimeLimitCPU      18000  % = 5 hours
ResubmitOn        0
ResubmitCommand   my-scriptfile


% Code options

ICFormat                 1
SnapFormat               1
ComovingIntegrationOn    1

TypeOfTimestepCriterion  0
OutputListOn             0
PeriodicBoundariesOn     1


%  Caracteristics of run

TimeBegin           0.015625   % Beginning of the simulation (z=63)
TimeMax	            8.0        % End of the simulation       (a= 8)

Omega0	            0.3  % only relevant for ComovingIntegrationOn = 1
OmegaLambda         0.7  % only relevant for ComovingIntegrationOn = 1
OmegaBaryon         0    % not used in the public version of GADGET-2
HubbleParam         0.7
BoxSize        150000.0


% Output frequency

TimeBetSnapshot        2.0
TimeOfFirstSnapshot    0.125

CpuTimeBetRestartFile     36000.0    ; here in seconds
TimeBetStatistics         0.125

NumFilesPerSnapshot       1
NumFilesWrittenInParallel 1


% Accuracy of time integration

ErrTolIntAccuracy      0.025

CourantFac             0.15

MaxSizeTimestep       0.01
MinSizeTimestep       0.0


% Tree algorithm, force accuracy, domain update frequency

ErrTolTheta            0.5            
TypeOfOpeningCriterion 1
ErrTolForceAcc         0.005


TreeDomainUpdateFrequency    0.1


%  Further parameters of SPH

DesNumNgb              50
MaxNumNgbDeviation     2
ArtBulkViscConst       0.8
InitGasTemp            0          % always ignored if set to 0 
MinGasTemp             0    


% Memory allocation

PartAllocFactor       1.5
TreeAllocFactor       0.8
BufferSize            25          % in MByte


% System of units

UnitLength_in_cm         3.085678e21        ; 1.0 kpc
UnitMass_in_g            1.989e43           ; 1.0e10 solar masses
UnitVelocity_in_cm_per_s 1e5                ; 1 km/sec
GravityConstantInternal  0                  ; should be set to 0 if ComovingIntegration = 1
 

% Softening lengths

MinGasHsmlFractional 0.25

% 33.48214 = 1/35 d/N = 1/35 150000/128
SofteningGas       33.48214
SofteningHalo      33.48214
SofteningDisk      33.48214
SofteningBulge     33.48214
SofteningStars     33.48214
SofteningBndry     33.48214

% Softening*MaxPhys is ignored if ComovingIntegration = 0
% set to a large value so that the comoving softening length always stays the same
% (i.e. Softening*MaxPhys / a > Softening*)
SofteningGasMaxPhys       1000.0
SofteningHaloMaxPhys      1000.0
SofteningDiskMaxPhys      1000.0
SofteningBulgeMaxPhys     1000.0
SofteningStarsMaxPhys     1000.0
SofteningBndryMaxPhys     1000.0


MaxRMSDisplacementFac 0.2

