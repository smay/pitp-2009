#!/bin/sh

path="${1:-../gadget-2/output_20cores}"

for p in $(ls -1 "$path"/snapshot*); do
	echo "$p"
	num=$(basename "$p" | sed s/snapshot_//)
	./FoF_Special ../gadget-2/output_20cores snapshot "$num"
done
