#!/usr/bin/env python3
# see exercise 1.3
# standard modules
import argparse
import os.path
# non-standard modules
import matplotlib.pyplot as plt
# own modules
import read_snapshot

parser = argparse.ArgumentParser(description='Make a dot plot (projection of '
	'the particles into the xy plane) of a Gadget-2 snapshot')
parser.add_argument('snapshot_files', nargs='+',
	help='paths to the files making up one Gadget-2 snapshot')
parser.add_argument('--longids', type=bool, default=True,
	help='whether to use 64-bit particle IDs instead of 32-bit ones '
		'(default: True)')
args = parser.parse_args()

first_header, particles = read_snapshot.read_snapshot(
	args.snapshot_files, args.longids
)

# plot the slab [0, L] × [0, L] × [0, L/5] (project along z axis)
L = first_header.BoxSize
pos = particles.pos
pos = pos[(pos[:, 2] >= 0) & (pos[:, 2] <= L/5)]

plt.title(f'$a = {first_header.time:.4g}$')
plt.xlabel('$x$ / $h^{-1}$ kpc')
plt.ylabel('$y$ / $h^{-1}$ kpc')
plt.xlim(0, L)
plt.ylim(0, L)

plt.plot(pos[:, 0], pos[:, 1], ',')

plt.savefig(
	f'{os.path.basename(args.snapshot_files[0])}.png', dpi=200,
	bbox_inches='tight'
)

