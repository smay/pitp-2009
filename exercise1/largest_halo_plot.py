#!/usr/bin/env python3
# see exercise 1.5
# standard modules
import argparse
# non-standard modules
import numpy
import matplotlib.pyplot as plt
# own modules
import read_snapshot

parser = argparse.ArgumentParser(description='Read a particle data for a '
	'group in the group catalog generated by FoF_Special')
parser.add_argument('path', help='the path to the Gadget-2 output directory')
parser.add_argument('snapshot_number', type=int,
	help='the number of the snapshot to be considered')
parser.add_argument('group_number', type=int, default=0, nargs='?',
	help='the number of the group to be considered (default: 0)')
parser.add_argument('--xlim', type=int, nargs=2,
	help='range of the x axis in the plot')
parser.add_argument('--ylim', type=int, nargs=2,
	help='range of the y axis in the plot')
args = parser.parse_args()

# read catalog analogous to ReadParticleData.pro
catalog_path = (
	f'{args.path}/groups_catalogue/'
	f'fof_special_catalogue_{args.snapshot_number:03}'
)
print(catalog_path)
with open(catalog_path, 'rb') as f:
	Ngroups = numpy.fromfile(f, 'u4', 1)[0]
	print(f'Ngroups = {Ngroups}')
	GroupLen = numpy.fromfile(f, 'u4', Ngroups)
	GroupOffset = numpy.fromfile(f, 'u4', Ngroups)
	GroupMass = numpy.fromfile(f, 'f4', Ngroups)
	GroupCM = numpy.fromfile(f, '3f4', Ngroups)
	GroupNSpecies = numpy.fromfile(f, '3u4', Ngroups)
	GroupMSpecies = numpy.fromfile(f, '3f4', Ngroups)
	GroupSfr = numpy.fromfile(f, 'f4', Ngroups)
	GroupMetallicities = numpy.fromfile(f, '2f4', Ngroups)
	Mcold = numpy.fromfile(f, 'f4', Ngroups)

# read particles file analogous to ReadParticleData.pro
GrNr = args.group_number
particles_path = (
	f'{args.path}/groups_particles/'
	f'fof_special_particles_{args.snapshot_number:03}'
)
print(particles_path)
with open(particles_path, 'rb') as f:
	Ntot = numpy.fromfile(f, 'u4', 1)[0]
	print(f'Ntot = {Ntot}')
	ParticlesPos = numpy.fromfile(f, '3f4', Ntot)
N = GroupLen[GrNr]
PP = ParticlesPos[GroupOffset[GrNr]:GroupOffset[GrNr] + N, :]

# read snapshot header
snapshot_path = f'{args.path}/snapshot_{args.snapshot_number:03}'
header = read_snapshot.read_header(snapshot_path)

# make a dot plot of the largest halo (PiTP 2009 exercise 1.5)
plt.title(f'$a$ = {header.time:.4g}')
plt.xlabel('$x$ / $h^{-1}$ kpc')
plt.ylabel('$y$ / $h^{-1}$ kpc')
# use same scale for x and y axes to prevent distortion of the image
plt.gca().set_aspect('equal')

plt.plot(PP[:, 0], PP[:, 1], ',')

if args.xlim:
	plt.xlim(args.xlim)
if args.ylim:
	plt.ylim(args.ylim)

plt.savefig(
	f's{args.snapshot_number:03}_g{GrNr}.png', dpi=200, bbox_inches='tight'
)

