import numpy

N_TYPES = 6
PARTICLE_TYPE_GAS = 0
HEADER_LEN = 256
HEADER_DTYPE = numpy.dtype([
	('npart',                 '6u4'),
	('mass',                  '6f8'),
	('time',                   'f8'),
	('redshift',               'f8'),
	('flag_sfr',               'i4'),
	('flag_feedback',          'i4'),
	('npartTotal',            '6u4'),
	('flag_cooling',           'i4'),
	('num_files',              'i4'),
	('BoxSize',                'f8'),
	('Omega0',                 'f8'),
	('OmegaLambda',            'f8'),
	('HubbleParam',            'f8'),
	('flag_stellarage',        'i4'),
	('flag_metals',            'i4'),
	('npartTotalHighWord',    '6u4'),
	('flag_entropy_instead_u', 'i4'),
])
PARTICLE_DTYPE = numpy.dtype([
	('type',     'i4'),
	('pos',     '3f4'),
	('vel',     '3f4'),
	('id',       'u8'),
	('mass',     'f4'),
	('U',        'f4'),
	('rho',      'f4'),
	('temp',     'f4'),
	('Ne',       'f4'),
	('num_file', 'u4'),
])

def read_block_len(snapshot_file):
	return numpy.fromfile(snapshot_file, 'i4', 1)[0]

def read_header(snapshot_file):
	# the longids setting doesn’t affect the header
	return read_snapshot([snapshot_file], particles=False)

def read_snapshot(snapshot_files, longids=False, particles=True):
	first_header = None
	curr_part = 0
	for num_file, file_name in enumerate(sorted(snapshot_files)):
		with open(file_name, 'rb') as snapshot:
			# block ID 1: HEAD
			block_len_1 = read_block_len(snapshot)
			header = numpy.rec.array(
				numpy.fromfile(snapshot, HEADER_DTYPE, 1)
			)[0]
			# skip padding
			snapshot.read(HEADER_LEN - HEADER_DTYPE.itemsize)
			block_len_2 = read_block_len(snapshot)
			assert block_len_1 == block_len_2 == HEADER_LEN
			if not particles:
				return header
			if first_header is None:
				first_header = header
				assert first_header.num_files == len(snapshot_files)
				bit_len = 2**(
					first_header.dtype['npartTotalHighWord']
						.subdtype[0].itemsize
				)
				npart_total_all = numpy.sum(
					(first_header.npartTotalHighWord << bit_len) +
					first_header.npartTotal
				)
				particles = numpy.rec.array(
					numpy.empty(npart_total_all, dtype=PARTICLE_DTYPE)
				)
				particles[:] = numpy.nan
			npart_all = sum(header.npart)
			npart_withmasses = sum(
				header.npart[p_type] for p_type in range(N_TYPES)
				if header.mass[p_type] == 0
			)
			# particle types and fixed masses
			offset = curr_part
			for p_type in range(N_TYPES):
				npart = header.npart[p_type]
				particles.type[offset:offset + npart] = p_type
				particles.mass[offset:offset + npart] = header.mass[p_type]
				offset += npart
			# store which file a particle was contained in
			particles.num_file[curr_part:curr_part + npart_all] = num_file
			# block ID 2: POS
			block_len_1 = read_block_len(snapshot)
			particles.pos[curr_part:curr_part + npart_all] = \
				numpy.fromfile(snapshot, '3f4', npart_all)
			block_len_2 = read_block_len(snapshot)
			assert block_len_1 == block_len_2 == \
				numpy.dtype('3f4').itemsize * npart_all
			# block ID 3: VEL
			block_len_1 = read_block_len(snapshot)
			particles.vel[curr_part:curr_part + npart_all] = \
				numpy.fromfile(snapshot, '3f4', npart_all)
			block_len_2 = read_block_len(snapshot)
			assert block_len_1 == block_len_2 == \
				numpy.dtype('3f4').itemsize * npart_all
			# block ID 4: ID
			block_len_1 = read_block_len(snapshot)
			id_dtype = 'u8' if longids else 'u4'
			particles.id[curr_part:curr_part + npart_all] = \
				numpy.fromfile(snapshot, id_dtype, npart_all)
			block_len_2 = read_block_len(snapshot)
			assert block_len_1 == block_len_2 == \
				numpy.dtype(id_dtype).itemsize * npart_all
			# block ID 5: MASS
			if npart_withmasses:
				block_len_1 = read_block_len(snapshot)
				mass_data = numpy.fromfile(snapshot, 'f4', npart_withmasses)
				offset_p = curr_part
				offset_m = curr_part
				for p_type in range(N_TYPES):
					npart = header.npart[p_type]
					if header.mass[p_type] == 0:
						particles.mass[offset_p:offset_p + npart] = \
							mass_data[offset_m:offset_m + npart]
						offset_m += npart
					offset_p += npart
				block_len_2 = read_block_len(snapshot)
				assert block_len_1 == block_len_2 == \
					numpy.dtype('f4').itemsize * npart_withmasses
			# gas blocks
			npart_gas = header.npart[PARTICLE_TYPE_GAS]
			if npart_gas:
				# block ID 6, 7, 8: U, RHO, HSML
				for col in 'U', 'rho', 'Ne':
					block_len_1 = read_block_len(snapshot)
					particles.U[curr_part:curr_part + npart_gas] = \
						numpy.fromfile(snapshot, 'f4', npart_gas)
					block_len_2 = read_block_len(snapshot)
					assert block_len_1 == block_len_2 == \
						numpy.dtype('f4').itemsize * npart_gas
		# update current particle offset
		curr_part += npart_all
	assert curr_part == npart_total_all, f'{curr_part} != {npart_total_all}'
	return first_header, particles

