#!/usr/bin/env python3
# standard modules
import argparse
# non-standard modules
import pandas
import matplotlib
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Plot the evolution of kinetic, '
	'potential and total energy with time')
parser.add_argument('path', help='the path to the Gadget-2 “energy.txt” '
	'output file')
args = parser.parse_args()

with open(args.path) as f:
	data = pandas.read_table(f, delim_whitespace=True, header=None)
data.columns = [
	['total'] * 4 +
	['internal_energy'] * 6 +
	['potential_energy'] * 6 +
	['kinetic_energy'] * 6 +
	['mass'] * 6,
	['time', 'internal_energy', 'potential_energy', 'kinetic_energy'] +
	list(range(6)) * 4
]
# discard highest-redshift output
data = data.iloc[1:, :]

# TODO comparison with Layzer–Irvine equation

plt.xlabel('$a$')
plt.ylabel('$E$ / $10^{10} M_\odot$ km$^2$ s$^{-2}$ $h^{-1}$')
plt.xscale('log')
plt.yscale('log')

plt.plot(data.total.time, data.total.kinetic_energy, label='$T$')
plt.plot(data.total.time, -data.total.potential_energy, label='$-V$')
plt.plot(data.total.time,
	-(data.total.kinetic_energy + data.total.potential_energy),
	label='$-(T + V)$'
)

plt.xlim(xmax=10)
plt.legend()

plt.savefig('energy.png', dpi=200, bbox_inches='tight')
plt.show()

