#!/bin/sh
set -x
module load idl

idl -e '.run make_dummy_glass' && \
make clean && \
make 'SYSTYPE="Freya"' && \
nice -n +20 mpiexec -np 1 ./N-GenIC exercise1.param && \
if [ $(ls -1 ICs/exercise1.dat*) -gt 1 ]; then
	cat ICs/exercise1.dat* > ICs/exercise1.dat
fi
cp ICs/* ~/gadget-2/Gadget-2.0.7/Gadget2/ICs/
