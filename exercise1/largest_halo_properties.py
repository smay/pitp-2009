#!/usr/bin/env python3
# see exercise 1.6
# standard modules
import argparse
import glob
import os.path
# non-standard modules
import numpy
import matplotlib
import matplotlib.pyplot as plt
# own modules
import read_snapshot

# matplotlib settings
matplotlib.rc('figure', dpi=200)

parser = argparse.ArgumentParser(description='Determine structural properties '
	'of the largest halo in exercise 1')
parser.add_argument('path', help='the path to the Gadget-2 output directory')
parser.add_argument('snapshot_number', type=int, nargs='?',
	help='the number of the snapshot to be considered (use all snapshots if '
		'not specified)')
parser.add_argument('--longids', type=bool, default=True,
	help='whether to use 64-bit particle IDs instead of 32-bit ones '
		'(default: True)')
args = parser.parse_args()

PARTICLE_TYPE_HALO = 1
GRNR_LARGEST = 0
GrNr = GRNR_LARGEST
SPHERE_FACTOR = 0.95

# get file paths
num = f'{args.snapshot_number:03}' if args.snapshot_number is not None else ''
catalog_paths = sorted(glob.glob(
	f'{args.path}/groups_catalogue/fof_special_catalogue_{num}*'
))
indexlist_paths = sorted(glob.glob(
	f'{args.path}/groups_indexlist/fof_special_indexlist_{num}*'
))
particles_paths = sorted(glob.glob(
	f'{args.path}/groups_particles/fof_special_particles_{num}*'
))
snapshot_paths = sorted(glob.glob(f'{args.path}/snapshot_{num}*'))
assert len(catalog_paths) == len(particles_paths) == len(snapshot_paths)

# figure for plot of velocity dispersion
fig_vel, ax_vel = plt.subplots()
ax_vel.set_xlabel(r'$r$ / Mpc')
ax_vel.set_ylabel(r'$\sigma$ / km s$^{-1}$')
ax_vel.set_xscale('log')
ax_beta = ax_vel.twinx()
ax_beta.set_ylabel(r'$\beta$')
ax_beta.axhline(c='gray', ls='--', lw=0.5)

# determine number of unique snapshots
a_values = set()
for snapshot in snapshot_paths:
	header = read_snapshot.read_header(snapshot)
	a_values.add(header.time)
cmap_vel = plt.get_cmap()
norm_vel = matplotlib.colors.LogNorm(vmin=min(a_values), vmax=max(a_values))
a_values = set()

for path_idx in range(len(snapshot_paths)):
	print(snapshot_paths[path_idx])
	# read current group catalog
	with open(catalog_paths[path_idx], 'rb') as f:
		Ngroups = numpy.fromfile(f, 'u4', 1)[0]
		# always choose first group (which is the largest)
		GroupLen = numpy.fromfile(f, 'u4', Ngroups)[GrNr]
		GroupOffset = numpy.fromfile(f, 'u4', Ngroups)[GrNr]
		GroupMass = numpy.fromfile(f, 'f4', Ngroups)[GrNr]
		GroupCM = numpy.fromfile(f, '3f4', Ngroups)[GrNr]
		GroupNSpecies = numpy.fromfile(f, '3u4', Ngroups)[GrNr]
		GroupMSpecies = numpy.fromfile(f, '3f4', Ngroups)[GrNr]
		GroupSfr = numpy.fromfile(f, 'f4', Ngroups)[GrNr]
		GroupMetallicities = numpy.fromfile(f, '2f4', Ngroups)[GrNr]
		Mcold = numpy.fromfile(f, 'f4', Ngroups)[GrNr]

	# read current particles file
	with open(particles_paths[path_idx], 'rb') as f:
		fof_ntot = numpy.fromfile(f, 'u4', 1)[0]
		fof_pos_all = numpy.fromfile(f, '3f4', fof_ntot)
	fof_pos = fof_pos_all[GroupOffset:GroupOffset + GroupLen, :]

	# read current snapshot file
	header, particles = read_snapshot.read_snapshot(
		[snapshot_paths[path_idx]], args.longids
	)

	# find the halo’s center
	pos = fof_pos
	# use maximum spatial extension along the coordinate axes as start radius
	radius = numpy.max(numpy.max(pos, axis=0) - numpy.min(pos, axis=0)) / 2
	while len(pos) > 1:
		# new center is center of mass of remaining particles
		# all particles have the same mass here, so the mass cancels
		center = numpy.sum(pos, axis=0) / len(pos)
		distances = numpy.linalg.norm(pos - center, axis=1)
		pos = pos[distances < radius]
		# shrink sphere
		radius *= SPHERE_FACTOR

	# physical units
	# using physical coordinates, i.e. multiply by scaling factor (header.time)
	a = header.time
	h = header.HubbleParam
	# convert from comoving, Gadget-internal units to Mpc
	units_length = a / h * 1e-3
	# convert from comoving, Gadget-internal units to km/sec
	units_vel = numpy.sqrt(a)
	# convert from Gadget-internal units to 10^10 M_s
	units_mass = 1 / h

	# determine bins for density profile
	center *= units_length
	# use all particles here, not only those in the FoF group
	distances = numpy.linalg.norm(
		particles.pos * units_length - center, axis=1
	)
	sort = numpy.argsort(distances)
	distances = distances[sort]
	# softening length: 33.48214 = 1/35 L/N = 1/35 150000/128
	softening_length = 33.48214 * units_length
	# determine virial radius (using r_vir = r_200)
	# Hubble constant is always 0.1 in Gadget-2 units
	H0 = 0.1
	Omega0, OmegaLambda, z = header.Omega0, header.OmegaLambda, header.redshift
	OmegaTot = Omega0 + OmegaLambda
	H2 = H0**2 * (
		Omega0 * (1 + z)**3 + (1 - OmegaTot) * (1 + z)**2 + OmegaLambda
	)
	# G in Gadget-2 units
	G = 43007.1
	# rho_vir = 200 * rho_c
	rho_vir = 200 * (3 * H2 / (8 * numpy.pi * G)) * (
		units_mass / units_length**3 * a**3
	)
	mass = header.mass[PARTICLE_TYPE_HALO]
	# spherically averaged densities
	rho_avg = mass * units_mass * (numpy.arange(len(particles)) + 1) / (
		4/3 * numpy.pi * distances**3
	)
	r_vir = distances[rho_avg >= rho_vir][-1]

	# plot density profile
	# logarithmic bins
	bins = numpy.geomspace(softening_length, 3 * r_vir, 20)
	rho_bins = numpy.empty(len(bins))
	for num, r in enumerate(bins):
		shell = rho_avg[distances < r]
		rho_bins[num] = shell[-1] if len(shell) else 0

	plt.figure()
	plt.title(f'$a = {a:.4g}$')
	plt.xlabel('$x$ / Mpc')
	plt.ylabel('$y$ / Mpc')
	plt.xlim(center[0] - 3 * r_vir, center[0] + 3 * r_vir)
	plt.ylim(center[1] - 3 * r_vir, center[1] + 3 * r_vir)
	ax = plt.gca()
	ax.set_facecolor('k')
	# use same scale for x and y axes to prevent distortion of the image
	ax.set_aspect('equal')

	cmap = plt.get_cmap('magma')
	norm = matplotlib.colors.LogNorm(
		vmin=min(rho_bins[rho_bins > 0]), vmax=max(rho_bins)
	)
	outer_circle = True
	for r, rho in zip(reversed(bins), reversed(rho_bins)):
		edge_color = 'w' if outer_circle else 'k'
		outer_circle = False
		if rho == 0: rho = norm.vmin
		ax.add_artist(plt.Circle(center[:2], r, color=cmap(norm(rho))))
		ax.add_artist(plt.Circle(center[:2], r, color=edge_color, fill=False))

	plt.plot(
		particles.pos[:, 0] * units_length,
		particles.pos[:, 1] * units_length, ',', color='cyan', alpha=0.6
	)

	plt.savefig(
		f'halo_density_{os.path.basename(snapshot_paths[path_idx])}.png',
		bbox_inches='tight'
	)

	# calculate center-of-mass velocity of the FoF group
	# For this, we need to read the current indexlist file because
	# unfortunately, the FoF group particles’ velocities are not stored
	# separately (unlike their positions).
	with open(indexlist_paths[path_idx], 'rb') as f:
		_ = numpy.fromfile(f, 'u4', 1)[0]
		assert _ == fof_ntot, f'{_} != {fof_ntot}'
		# important: particle indices are one-based in indexlist!
		fof_indices_all = numpy.fromfile(f, 'u4', fof_ntot) - 1
	fof_indices = fof_indices_all[GroupOffset:GroupOffset + GroupLen]
	assert numpy.all(particles.pos[fof_indices] == fof_pos)

	fof_vel = particles.vel[fof_indices]
	# all particles have the same mass here, so the mass cancels
	v_com = numpy.sum(fof_vel, axis=0) / len(fof_vel)

	# measure velocity dispersion
	pos_sorted = particles.pos[sort] * units_length
	vel_sorted = (particles.vel[sort] - v_com) * units_vel
	sigma2_r = []
	sigma2_t = []
	beta = []
	r_bin_prev = bins[0]
	for num, r_bin in enumerate(bins):
		if num == 0: continue
		mask = (r_bin_prev <= distances) & (distances < r_bin)
		pos = pos_sorted[mask]
		vel = vel_sorted[mask]
		if len(pos) < 2:
			sigma2_r.append(0)
			sigma2_t.append(0)
			beta.append(numpy.nan)
			continue
		# calculate position components and unit vectors in spherical
		# coordinates
		r = numpy.linalg.norm(pos, axis=1)
		theta = numpy.arccos(pos[:, 2] / r)
		phi = numpy.arctan2(pos[:, 1], pos[:, 0])
		e_r = pos / numpy.vstack(r)
		e_theta = numpy.column_stack((
			numpy.cos(theta) * numpy.cos(phi),
			numpy.cos(theta) * numpy.cos(phi),
			-numpy.sin(theta)
		))
		e_phi = numpy.column_stack((
			-numpy.sin(phi), numpy.cos(phi), numpy.zeros(len(phi))
		))
		# calculate velocity components parallel to position vectors:
		# v_c_i = vel_i ⋅ e_c_i
		# equivalent to numpy.diag(numpy.inner(vel, e_c))
		v_r = numpy.einsum('ij,ij->i', vel, e_r)
		v_theta = numpy.einsum('ij,ij->i', vel, e_theta)
		v_phi = numpy.einsum('ij,ij->i', vel, e_phi)
		# calculate velocity dispersions
		sigma2_r.append(numpy.var(v_r))
		sigma2_theta = numpy.var(v_theta)
		sigma2_phi = numpy.var(v_phi)
		sigma2_t.append((sigma2_theta + sigma2_phi) / 2)
		beta.append(1 - sigma2_t[-1] / sigma2_r[-1])
		r_bin_prev = r_bin

	# plot velocity dispersion
	if a not in a_values:
		a_values.add(a)
		bins = numpy.array(bins)
		bins = (bins[1:] + bins[:-1]) / 2
		ax_vel.plot(bins, numpy.sqrt(sigma2_r), c=cmap_vel(norm_vel(a)),
			label=f'$a = {a:.4g}$', lw=1)
		ax_vel.plot(bins, numpy.sqrt(sigma2_t), '--', c=cmap_vel(norm_vel(a)),
			lw=1)
		ax_beta.plot(bins, beta, ':', c=cmap_vel(norm_vel(a)), lw=0.5)

# save velocity dispersion plot
ax_vel.set_ylim(bottom=0)
ax_beta.set_ylim(-1.5, 1.5)
ax_vel.legend()
fig_vel.savefig('halo_velocity_dispersion.png', bbox_inches='tight')

