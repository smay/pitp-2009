#!/usr/bin/env python3
# see exercise 1.5
# standard modules
import argparse
# non-standard modules
import numpy
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Read a group catalog generated '
	'by FoF_Special')
#parser.add_argument('path', help='the path to the Gadget-2 output directory')
#parser.add_argument('number', type=int, default=0,
#	help='the number of the snapshot to be considered')
parser.add_argument('paths', nargs='*',
	help='paths to FoF_Special catalog files')
args = parser.parse_args()

# read catalog analogous to ReadCatalogue.pro
#catalog_path = (
#	f'{args.path}/groups_catalogue/fof_special_catalogue_{args.number:03}'
#)
for catalog_path in args.paths:
	print(catalog_path)
	with open(catalog_path, 'rb') as f:
		Ngroups = numpy.fromfile(f, 'u4', 1)[0]
		print(f'Ngroups = {Ngroups}')
		GroupLen = numpy.fromfile(f, 'u4', Ngroups)
		GroupOffset = numpy.fromfile(f, 'u4', Ngroups)
		GroupMass = numpy.fromfile(f, 'f4', Ngroups)
		GroupCM = numpy.fromfile(f, '3f4', Ngroups)
		GroupNSpecies = numpy.fromfile(f, '3u4', Ngroups)
		GroupMSpecies = numpy.fromfile(f, '3f4', Ngroups)
		GroupSfr = numpy.fromfile(f, 'f4', Ngroups)
		GroupMetallicities = numpy.fromfile(f, '2f4', Ngroups)
		Mcold = numpy.fromfile(f, 'f4', Ngroups)
	Ngas   = GroupNSpecies[:, 0]
	Ndm    = GroupNSpecies[:, 1]
	Nstars = GroupNSpecies[:, 2]
	Mgas   = GroupMSpecies[:, 0]
	Mdm    = GroupMSpecies[:, 1]
	Mstars = GroupMSpecies[:, 2]
	Zgas   = GroupMetallicities[:, 0]
	Zstars = GroupMetallicities[:, 1]
	if Ngroups > 0:
		print(f'Largest group has {GroupLen[0]} particles.')
		print(f'Gas:       {Ngas[0]}')
		print(f'DM:        {Ndm[0]}')
		print(f'Stars:     {Nstars[0]}')
		print(f'<Zgas>   = {Zgas[0]}')
		print(f'<Zstars> = {Zstars[0]}')
	print()


	# plot cumulative mass function (PiTP 2009 exercise 1.5)
	masses = numpy.append(Mdm, 0)
	counts = numpy.arange(Ngroups, -1, -1)

	plt.plot(masses, counts, label=f'snapshot {catalog_path[-3:]}')

plt.xlabel('$M$ / $10^{10} M_\odot h^{-1}$')
plt.ylabel('$N$')
plt.xlim(xmin=0)
plt.ylim(ymin=0)
plt.legend()

plt.show()

