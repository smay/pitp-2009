  N = 1000L

  rho = 1.0
  pressure = 1.0
  gamma = 5.0/3

  Box = 1.0


  pos = fltarr(3, N)
  vel = fltarr(3, N)
  id = lindgen(N)+1
  press = fltarr(N)


  pos(0, *) = Box * randomu(seed, N)
  pos(1, *) = Box * randomu(seed, N)



  npart=lonarr(6)
  massarr=dblarr(6)
  time=0.0D
  redshift=0.0D
  flag_sfr=0L
  flag_feedback=0L
  npartTotal=lonarr(6)
  flag_cooling=0L
  num_files = 1L
  BoxSize=double(box)
  Omega0=0.0D
  OmegaLambda=0.0D
  HubbleParam=0.0D
  flag_stellarage=0L
  flag_metals=0L
  highwork=lonarr(6)
  flag_entropy=0L
  flag_double=0L
  flag_lpt=0L
  factor=0.0
  la=intarr(48/2)

  npart(0) = N
  npartTotal(0) = N

  massarr(0) = rho * Box^2 / N

  u = fltarr(N)
  u(*) = pressure / (gamma-1) / rho



  openw, 1, "ics_"+strcompress(string(N), /remove_all)+".dat", /f77_unformatted

  writeu,1,npart,massarr,time,redshift,flag_sfr,flag_feedback,npartTotal, $
         flag_cooling, num_files, BoxSize, Omega0, OmegaLambda, HubbleParam, flag_stellarage, flag_metals, highwork,$
         flag_entropy, flag_double, flag_lpt, factor, la

  writeu,1,pos
  writeu,1,vel
  writeu,1,id
  writeu,1,u
  close,1


end
